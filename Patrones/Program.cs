﻿using Patrones.FabricaAbstracta;
using Patrones.FabricaAbstracta.FabricaConcretaAliens;
using Patrones.FabricaAbstracta.FabricaConcretaHumanos;
using System;
using System.Threading;

namespace Patrones
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("COMENZANDO EL JUEGO...\n\n");
            Thread.Sleep(2000);
            Console.WriteLine("CREANDO JUGADOR HUMANO...");

            IFabricaJugadores fabricaHumanos = new FabricaHumanos();
            IJugador jugadorHumano = fabricaHumanos.CrearJugador();
            IArma armaHumano = fabricaHumanos.CrearArma();
            jugadorHumano.Arma = armaHumano;

            Thread.Sleep(2000);
            Console.WriteLine("Jugador humano creado: '" + jugadorHumano.Nombre + "'");
            Console.WriteLine("Vida jugador humano: " + jugadorHumano.Vida);
            Console.WriteLine("Fuerza jugador humano: " + jugadorHumano.Fuerza);
            Console.WriteLine("Arma humana creada: " + jugadorHumano.Arma.Nombre);

            Thread.Sleep(2000);
            Console.WriteLine("\n\nCREANDO JUGADOR ALIEN...");

            IFabricaJugadores fabricaAliens = new FabricaAliens();
            IJugador jugadorAlien = fabricaAliens.CrearJugador();
            IArma armaAlien = fabricaAliens.CrearArma();
            jugadorAlien.Arma = armaAlien;

            Thread.Sleep(2000);
            Console.WriteLine("Jugador alien creado: '" + jugadorAlien.Nombre + "'");
            Console.WriteLine("Vida jugador alien: " + jugadorAlien.Vida);
            Console.WriteLine("Fureza jugador alien: " + jugadorAlien.Fuerza);
            Console.WriteLine("Arma alien creada: " + jugadorAlien.Arma.Nombre);

            Thread.Sleep(2000);

            Console.WriteLine("\n\nCOMIENZA EL ATAQUE...\n\n");
            Console.WriteLine("Jugador humano ataca...");
            Thread.Sleep(2000);
            jugadorHumano.Atacar(jugadorAlien);
            Console.WriteLine($"Jugador alien tiene {jugadorAlien.Vida} de vida");
            Thread.Sleep(2000);
            Console.WriteLine("\n\nJugador alien ataca...");
            Thread.Sleep(2000);
            jugadorAlien.Atacar(jugadorHumano);
            Console.WriteLine($"Jugador humano tiene {jugadorHumano.Vida} de vida");
            Thread.Sleep(2000);

            Console.ReadKey();
        }
    }
}
