﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrones.FabricaAbstracta.FabricaConcretaHumanos
{
    public class FabricaHumanos : IFabricaJugadores
    {
        Random random = new Random();

        public IArma CrearArma()
        {
            return new ArmaHumano
            {
                Nombre = "Pistola",
                Daño = 8
            };
        }

        public IJugador CrearJugador()
        {
            return new Humano
            {
                Nombre = "Humano " + random.Next(100, 500),
                Velocidad = 30,
                Fuerza = random.Next(30, 40),
                TieneEscudo = true,
                Vida = 100
            };
        }
    }
}
