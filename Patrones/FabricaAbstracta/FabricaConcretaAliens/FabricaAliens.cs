﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrones.FabricaAbstracta.FabricaConcretaAliens
{
    public class FabricaAliens : IFabricaJugadores
    {
        Random random = new Random();
        public IArma CrearArma()
        {
            return new ArmaAlien
            {
                Nombre = "Plasma",
                Daño = 10
            };
        }

        public IJugador CrearJugador()
        {
            return new Alien
            {
                Nombre = "Alien " + random.Next(600, 1000),
                Velocidad = 90,
                Fuerza = random.Next(50, 60),
                TieneEscudo = false,
                Vida = 90
            };
        }
    }
}
