﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrones.FabricaAbstracta.FabricaConcretaAliens
{
    public class ArmaAlien : IArma
    {
        public string Nombre { get; set; }
        public double Daño { get; set; }
    }
}
