﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrones.FabricaAbstracta.FabricaConcretaAliens
{
    public class Alien : IJugador
    {
        public string Nombre { get; set; }
        public int Velocidad { get; set; }
        public int Fuerza { get; set; }
        public bool TieneEscudo { get; set; }
        public double Vida { get; set; }
        public IArma Arma { get; set; }

        public void Atacar(IJugador contrincante)
        {
            double golpe = Fuerza * 0.20; // Solo ataca con el 20% de su fuerza
            golpe += Arma.Daño;

            if (contrincante.TieneEscudo)
            {
                Console.WriteLine($"El ataque destruyó el escudo de {contrincante.Nombre}...");
                contrincante.Vida -= golpe * 0.7;
                contrincante.TieneEscudo = false;
            }
            else
            {
                Console.WriteLine($"El ataque disminuyó la vida de {contrincante.Nombre}...");
                contrincante.Vida -= golpe;
            }
        }
    }
}
