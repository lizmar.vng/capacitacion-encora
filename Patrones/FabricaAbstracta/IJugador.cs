﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrones.FabricaAbstracta
{
    public interface IJugador
    {
        string Nombre { get; set; }
        int Velocidad { get; set; }
        int Fuerza { get; set; }
        bool TieneEscudo { get; set; }
        double Vida { get; set; }
        IArma Arma { get; set; }

        void Atacar(IJugador contrincante);
    }
}
