﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrones.FabricaAbstracta
{
    public interface IArma
    {
        string Nombre { get; set; }
        double Daño { get; set; }
    }
}
